package it.keybiz.intranet.prenotazioni.controllers;

import it.keybiz.intranet.prenotazioni.entities.Authority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface AuthorityController {

    @PostMapping
    Authority addNewWeighedAuthority(@RequestParam String authority, @RequestParam int peso);

    @PutMapping
    Authority modifyWeight(@RequestParam String authority, @RequestParam int nuovoPeso);
}
