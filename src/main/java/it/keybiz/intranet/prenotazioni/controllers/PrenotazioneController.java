package it.keybiz.intranet.prenotazioni.controllers;

import it.keybiz.intranet.prenotazioni.controllers.bodies.PrenotazioneBody;
import it.keybiz.intranet.prenotazioni.entities.Prenotazione;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

public interface PrenotazioneController {

    @GetMapping
    Collection<Prenotazione> listaPrenotazioni();

    @GetMapping("attive")
    Collection<Prenotazione> listPrenotazioniActives();

    @GetMapping("specifica")
    Prenotazione getByID(@RequestParam String prenotazione);

    @GetMapping("prenotazioni-utente")
    Collection<Prenotazione> prenotazioniDellUtente(@RequestParam String utente);

    @GetMapping("prenotazioni-attive-utente")
    Collection<Prenotazione> prenotazioniAttiveDellUtente(@RequestParam String utente);

    @PostMapping
    Prenotazione prenotaUnaStanza(@RequestParam String utente, @RequestBody PrenotazioneBody prenotazioneBody);

    @PostMapping("precedenza")
    Prenotazione createNewPriorityPrenotazione(@RequestBody PrenotazioneBody prenotazioneBody, @RequestParam String utente);

    @DeleteMapping
    Prenotazione deletePrenotazione(@RequestParam String prenotazione);

    @PutMapping
    Prenotazione updatePrenotazione(@RequestParam String prenotazione, @RequestParam String utente, @RequestBody PrenotazioneBody prenotazioneBody);

    @PutMapping("annulla")
    Prenotazione cancelPrenotazione(@RequestParam String prenotazione, @RequestParam String utente);
}
