package it.keybiz.intranet.prenotazioni.controllers;

import it.keybiz.intranet.prenotazioni.controllers.bodies.StanzaBody;
import it.keybiz.intranet.prenotazioni.entities.Stanza;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

public interface StanzaController {

    @GetMapping
    Page<Stanza> listaStanze(@PageableDefault Pageable pageable);

    @GetMapping("specifica")
    Stanza getByID(@RequestParam String stanza);

    @DeleteMapping
    Stanza deleteStanza(@RequestParam String stanza);

    @PostMapping
    Stanza createNewStanza(@RequestBody StanzaBody stanzaBody);

    @PutMapping
    Stanza updateStanza(@RequestParam String stanza, @RequestBody StanzaBody stanzaBody);

}
