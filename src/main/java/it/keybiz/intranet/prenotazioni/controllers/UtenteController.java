package it.keybiz.intranet.prenotazioni.controllers;

import it.keybiz.intranet.prenotazioni.entities.Utente;
import it.keybiz.intranet.prenotazioni.entities.UtenteConNome;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface UtenteController {

    @GetMapping("inaffidabile")
    List<UtenteConNome> getUsersMoreUntrustable(@RequestParam int numeroTotaleErrori);

    @PutMapping("unlock")
    Utente unlockUser(@RequestParam String utente);
}
