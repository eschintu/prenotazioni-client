package it.keybiz.intranet.prenotazioni.controllers.bodies;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class PrenotazioneBody {

    private boolean prenotazionePrioritaria;

    private boolean prenotazioneConCibo;

    @NotBlank
    private String dataInizioPrenotazione;

    @NotBlank
    private String oraInizioPrenotazione;

    @NotBlank
    private String dataFinePrenotazione;

    @NotBlank
    private String oraFinePrenotazione;

    private String note;

    @NotBlank
    private String stanza;
}
