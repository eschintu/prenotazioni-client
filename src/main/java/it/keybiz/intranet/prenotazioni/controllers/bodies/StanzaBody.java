package it.keybiz.intranet.prenotazioni.controllers.bodies;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StanzaBody {

    @NotBlank
    private String codice;

    @NotNull
    private int numeriPostiDisponibili;

    @NotNull
    private int dimensioneMQ2;

    @NotNull
    private boolean connessioneInternet;

    @NotNull
    private boolean dispositivoVideo;

    @NotNull
    private boolean dispositivoCallConference;
}
