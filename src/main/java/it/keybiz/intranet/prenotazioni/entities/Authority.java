package it.keybiz.intranet.prenotazioni.entities;

import it.keybiz.intranet.common.entities.AbstractIdentifiableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class Authority extends AbstractIdentifiableEntity {

    @Column(nullable = false)
    private int peso;

}
