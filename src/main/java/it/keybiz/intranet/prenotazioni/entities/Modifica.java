package it.keybiz.intranet.prenotazioni.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.keybiz.intranet.common.entities.AbstractIdentifiableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class Modifica extends AbstractIdentifiableEntity {

    @Column(nullable = false)
    private TipologiaModifica tipologiaModifica;

    @Column(nullable = false)
    private LocalDateTime momentoAvvenutaModifica;

    @Column(nullable = false)
    private String note;

    @ManyToOne
    @JsonIgnore
    private Prenotazione prenotazione;
}