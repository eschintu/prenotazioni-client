package it.keybiz.intranet.prenotazioni.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.keybiz.intranet.common.entities.AbstractIdentifiableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Getter
@Setter
public class Prenotazione extends AbstractIdentifiableEntity {

    private boolean prenotazionePrioritaria;

    private boolean prenotazioneConCibo;

    @Column(nullable = false)
    private LocalDateTime dataOraEffetuataPrenotazione;

    @Column(nullable = false)
    private LocalDateTime dataOraInizioPrenotazione;

    @Column(nullable = false)
    private LocalDateTime dataOraFinePrenotazione;

    private LocalDateTime dataOraAnnullamentoPrenotazione;

    private String note;

    @ManyToOne
    @JsonIgnore
    private Stanza stanzaPrenotata;

    @ManyToOne
    @JsonIgnore
    private Utente utente;

    @OneToMany(mappedBy = "prenotazione")
    private Collection<Modifica> modificheSubite;
}
