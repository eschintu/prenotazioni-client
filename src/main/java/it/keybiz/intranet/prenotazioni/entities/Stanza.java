package it.keybiz.intranet.prenotazioni.entities;

import it.keybiz.intranet.common.entities.AbstractIdentifiableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
@Getter
@Setter
public class Stanza extends AbstractIdentifiableEntity {

    @Column(nullable = false)
    private String codice;

    @Column(nullable = false)
    private int numeriPostiDisponibili;

    @Column(nullable = false)
    private int dimensioneMQ2;

    @Column(nullable = false)
    private boolean connessioneInternet;

    @Column(nullable = false)
    private boolean dispositivoVideo;

    @Column(nullable = false)
    private boolean dispositivoCallConference;

    @OneToMany(mappedBy = "stanzaPrenotata")
    private Collection<Prenotazione> prenotazioniFatte;
}
