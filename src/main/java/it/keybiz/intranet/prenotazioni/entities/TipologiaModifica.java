package it.keybiz.intranet.prenotazioni.entities;

import lombok.Getter;

@Getter
public enum TipologiaModifica {
    MODIFICA_BUONA,
    MODIFICA_CATTIVA,
    CANCELLAZIONE_CATTIVA,
    CANCELLAZIONE_BUONA
}
