package it.keybiz.intranet.prenotazioni.entities;

import it.keybiz.intranet.common.entities.AbstractIdentifiableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
@Getter
@Setter
public class Utente extends AbstractIdentifiableEntity {

    private int cancellazioniCattive;

    private int modificheCattive;

    @Column(nullable = false)
    private boolean listaNeraPrenotazioni;

    @OneToMany(mappedBy = "utente")
    private Collection<Prenotazione> prenotazioniEffettuate;

    @OneToMany(mappedBy = "utente")
    private Collection<SbloccoUtente> storicoSblocchiUtente;
}