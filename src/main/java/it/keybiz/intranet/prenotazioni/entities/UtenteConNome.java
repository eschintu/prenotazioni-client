package it.keybiz.intranet.prenotazioni.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UtenteConNome {

    private String nome;

    private String cognome;

    private int numeroModifichePrenotazioni;

    private int numeroCancellazionePrenotazioni;

    private int numeroSblocchiAccount;

}
